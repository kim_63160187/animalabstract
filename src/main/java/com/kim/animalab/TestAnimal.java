/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.animalab;

/**
 *
 * @author ASUS
 */
public class TestAnimal {
    public static void main(String[] args) {
        
        Human h1 = new Human("King");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));
        
        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Cat c1 = new Cat("Malee");
        c1.eat();
        c1.walk();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));
        
        Animal a2 = c1;
        System.out.println("c1 is land animal ? " + (a2 instanceof LandAnimal));
        System.out.println("c1 is poultry animal ? " + (a2 instanceof Poultry));
      
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Dog d1 = new Dog("Ball");
        d1.eat();
        d1.walk();
        d1.run();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));
        
        Animal a3 = d1;
        System.out.println("d1 is land animal ? " + (a3 instanceof LandAnimal));
        System.out.println("d1 is poultry animal ? " + (a3 instanceof Poultry));
        System.out.println("d1 is aquatic animal ? " + (a3 instanceof AquaticAnimal));
        
      
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Crocodile r1 = new Crocodile("Micky");
        r1.crawl();
        r1.eat();
        r1.walk();
        r1.sleep();
        System.out.println("r1 is animal ? " + (r1 instanceof Animal));
        System.out.println("r1 is reptile animal ? " + (r1 instanceof Reptile));
        
        Animal a4 = r1;
        System.out.println("r1 is reptile animal ? " + (a4 instanceof Reptile));
        System.out.println("r1 is poultry animal ? " + (a4 instanceof Poultry));
        System.out.println("r1 is land animal ? " + (a4 instanceof LandAnimal));
        
      
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Snake s1 = new Snake("Sky");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.sleep();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile animal ? " + (s1 instanceof Reptile));
        
        Animal a5 = s1;
        System.out.println("s1 is reptile animal ? " + (a5 instanceof Reptile));
        System.out.println("s1 is aquatic animal ? " + (a5 instanceof AquaticAnimal));
        System.out.println("s1 is poultry animal ? " + (a5 instanceof Poultry));
        System.out.println("s1 is land animal ? " + (a5 instanceof LandAnimal));
        
      
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Fish f1 = new Fish("Gold");
        f1.swim();
        f1.eat();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ? " + (f1 instanceof AquaticAnimal));
        
        Animal a6 = f1;
        System.out.println("f1 is aquatic animal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("f1 is poultry animal ? " + (a6 instanceof Poultry));
        System.out.println("f1 is land animal ? " + (a6 instanceof LandAnimal));
        
      
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Crab z1 = new Crab("Red");
        z1.swim();
        z1.eat();
        z1.speak();
        z1.sleep();
        System.out.println("z1 is animal ? " + (z1 instanceof Animal));
        System.out.println("z1 is aquatic animal ? " + (z1 instanceof AquaticAnimal));
        
        Animal a7 = a1;
        System.out.println("z1 is aquatic animal ? " + (a7 instanceof AquaticAnimal));
        System.out.println("z1 is poultry animal ? " + (a7 instanceof Poultry));
        System.out.println("z1 is land animal ? " + (a7 instanceof LandAnimal));
        System.out.println("z1 is reptile animal ? " + (a7 instanceof Reptile));
        
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Bird w1 = new Bird("Tiger");
        w1.fly();
        w1.eat();
        w1.speak();
        w1.sleep();
        System.out.println("w1 is animal ? " + (w1 instanceof Animal));
        System.out.println("w1 is poultry animal ? " + (w1 instanceof Poultry));
        
        Animal a8 = w1;
        System.out.println("w1 is poultry animal ? " + (a8 instanceof Poultry));
        System.out.println("w1 is land animal ? " + (a8 instanceof LandAnimal));
        System.out.println("w1 is reptile animal ? " + (a8 instanceof Reptile));
        
        System.out.println("---------------------------------------------------------------------------------------------");
        
        Bat t1 = new Bat("Batman");
        t1.fly();
        t1.eat();
        t1.sleep();
        System.out.println("t1 is animal ? " + (t1 instanceof Animal));
        System.out.println("t1 is poultry animal ? " + (t1 instanceof Poultry));
        
        Animal a9 = t1;
        System.out.println("t1 is poultry animal ? " + (a9 instanceof Poultry));
        System.out.println("t1 is reptile animal ? " + (a9 instanceof Reptile));
    }
}
